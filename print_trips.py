import csv

daysofweek=["Mon","Tue","Wed","Thu","Fri","Sat","Sun"]


op_type = raw_input("Enter operator code: ")

# Read and process all the gtfs files  
agency_file=csv.reader(open("agency.csv", "r"),delimiter=",")
stops_file = csv.reader(open("stops.csv", "r"),delimiter=",")
trips_file = csv.reader(open("trips.csv", "r"),delimiter=",")
routes_file= csv.reader(open("routes.csv","r"),delimiter=",")
stoptimes_file = csv.reader(open("stop_times.csv", "r"), delimiter= ",")
calendar_file=csv.reader(open("calendar.csv", "r"), delimiter= ",")


#Creates an empty dictionaries for trips and routes 
agency={}
trips= {}
routes={}
stops={}
stop_times={}
calendar={}

#Read in stops file and build kye/value pairs for stop_id and stop_desc
for st in stops_file:
    stops[st[0]]=st[3]
    
#Read in agency file and build key/value pairs of agency_id and agency_name 
for ag in agency_file:
    agency[ag[0]] = ag[1]
    
#Read in routes file and build key/value pairs of routeId and agencyId
for rt in routes_file:
    routes[rt[0]] = rt[1]

#Read in calendar file and build key/value pairs of serviceID and dates
for cal in calendar_file:
    calendar[cal[0]]=[cal[1],cal[2],cal[3],cal[4],cal[5],cal[6],cal[7]]

#Read in stop_times file 
#tripID field of the format: TA0101_W9-201 
for stt in stoptimes_file:
    week=''
    if stt[0]!= 'trip_id':          #ignore header row
        routecode= "R"+stt[0][1:6]  #extract the routeID from trip_id field of stop_times file 
        agencyId=routes[routecode]  #extract the agency based on the routecode 
        service=stt[0][7:]          #extract service_id from trip_id field 
        stop=stt[3]                 #stopId is the 4th field in stop_times 
        
        #print op_type, agencyId
        if op_type== agencyId:    
            #Print trip summary at the start of the trip, i.e. stop_sequence = 1
            if stt[4] == "1":           
                #convert calendar into days
                for i in range(0,len(calendar[service])):
                    if calendar[service][i] == "1": 
                        week=week+daysofweek[i]
                if week == "MonTueWedThuFriSatSun":
                    week = "Daily"

                print "\n", agency[agencyId], ",", service, ",", week 
                print stops[stop],',',stt[1]
            else:
                if len(stt[1])>5:   #reformat HH:MM for times >24hr clock
                    hr1=int(stt[1][:2])-24
                    hrstr1="0"+str(hr1)+":"+stt[1][3:5]
                    hr2=int(stt[2][:2])-24
                    hrstr2="0"+str(hr2)+":"+stt[2][3:5]
                    if hrstr1 == hrstr2:
                        print stops[stop],',', hrstr1 
                    else:
                        print stops[stop],',',hrstr1,',',hrstr2
                else:
                    if stt[1] == stt[2]:
                        print stops[stop],',',stt[1]
                    else:
                        print stops[stop],',',stt[1],',',stt[2]



# with open(out_file,'w') as csvfile:
    # annotated = csv.writer(csvfile,delimiter=',')
    
    # for trrow in stoptimes_f:
        # # Writes out to output file the same values from stop_times but stop_id field is 
        # # replaced by concatinating stop_id abd stop_code (from stops file)
        # stimesnew.writerow([newtrid[trrow[0]],trrow[1],trrow[2],trrow[3],trrow[4]])
