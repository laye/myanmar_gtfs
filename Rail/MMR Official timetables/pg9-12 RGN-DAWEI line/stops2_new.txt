stop_id,stop_code,stop_name,stop_desc,stop_lat,stop_lon,zone_id,stop_url,location_type,parent_station,stop_timezone,wheelchair_boarding
STR00001_RRGN,1,Yangon Central Station,,16.781174,96.161419,,,,,,
STR00002_RPZT,2,Puzundaung Station,,16.786415,96.175177,,,,,,
STR00003_RMLK,3,Ma Hlwa Gone Station,,16.802689,96.18162,,,,,,
STR00004_RTGK,4,Thin Gan Gyun Station,,16.828493,96.196376,,,,,,
STR00005_RTTK,5,Toe Kyaung Ka Lay Station,,16.862804,96.215116,,,,,,
STR00006_RYTG,6,Ywar Thar Gyi Station,,16.931075,96.264905,,,,,,
