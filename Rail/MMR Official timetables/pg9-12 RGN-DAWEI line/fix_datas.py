# File to update either:-
# a stop_id from stops.txt and stop_times.txt files.
# a trip_id from routes.txt and stop_times.txt files.
#
# usage> fix_data.py -c <stop_id|trip_id> -o <old value> -n <new value>

import argparse, sys, os
import re

def fix_data(cat,old_vals,new_vals):
	print old_vals
	file2 = 'stop_times.txt'
	if cat == "stop":
		file1 = 'stops2.txt'
	else:
		file1 = 'routes.txt'
		
	try:
		fh1 = open(file1)
	except:
		print 'File cannot be found: ', file1
		exit()
#	try:
#		fh2 = open(file2)
#	except:
#		print 'File cannot be found: ', file2
#		exit()


		
	print 'open file'
	fo1 = file1.split('.')
	fo2 = file2.split('.')	
	fout1 = ''.join((fo1[0],'_new.txt'))
	fout2 = ''.join((fo2[0],'_new.txt'))
	
	#process file 1
	fhand1 = open(fout1,'wb')
	if cat == "stop":
		#clean up stops file
		
		for line in fh1:
			newln = line
			if 'stop_id' in line: 
				fhand1.write(line)
			else:
				stopid = re.findall('^S\w*',line) #search for regex pattern mathing 'STRxxx'
				if ''.join(stopid) in old_vals:
					print 'matched found'
					rest_of_line = ''.join(re.findall('^S\w*(.*)',line))
					print rest_of_line
					newln = ''.join((new_val,rest_of_line,'\n'))
				fhand1.write(newln)
				print newln
		fhand1.close()

	
	fhand2 = open(fout2,'wb')

if __name__ == '__main__':

	parser = argparse.ArgumentParser()

	parser.add_argument("-c", "--category", dest="cat", help="Category (stop or trip)", metavar="STRING")
	parser.add_argument("-o", "--old values", dest="old_vals", help="Old Values", metavar="LIST")
	parser.add_argument("-n", "--new values", dest="new_vals", help="New Values", metavar="STRING")		

	args=parser.parse_args()
	fix_data(args.cat,args.old_vals,args.new_vals)

