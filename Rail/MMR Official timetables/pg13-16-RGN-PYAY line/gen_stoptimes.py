# File to read in a simplified timetable and 
# outputs a file in gtfs stop_times.txt format
# ready for processing.
#
# usage >python gen_stop_times.py -t <trip_id> -i <inpu-file.csv>


import argparse, sys, os

def gen_stop_times(trip_id,infile):

	try:
		fh = open(infile)
	except:
		print 'File cannot be found: ', infile
		exit()
		
	fnew = infile.split('.')
	fout = ''.join((fnew[0],'.txt'))
	fhand = open(fout,'wb')

	lastdep=0
	for line in fh:
		if 'stop_id' in line: 
			fhand.write('trip_id,arrival_time,departure_time,stop_id,stop_sequence,stop_headsign,pickup_type,drop_off_type,shape_dist_traveled,timepoint\n')
		else:
			line = line.rstrip()
			rec = line.split(',')
			dep = rec[1]
			arr = rec[2]
			if lastdep > dep:
				dephr=str(int(dep[:2])+24)
				arrhr=str(int(arr[:2])+24)
			else:
				dephr=dep[:2]
				arrhr=arr[:2]		
				lastdep = dep
			newdep = ''.join((dephr,':',dep[2:],':00,'))
			newarr = ''.join((arrhr,':',arr[2:],':00,'))
			newln = ''.join((trip_id,',',newdep,newarr,rec[0],',',rec[3],',,,,,\n'))
			fhand.write(newln)
			print newln
	fhand.close()


if __name__ == '__main__':

        parser = argparse.ArgumentParser()

        parser.add_argument("-t", "--trip_id", dest="trip_id", help="trip_id from trips file", metavar="STRING")
        parser.add_argument("-f", "--file", dest="infile", help="Input file", metavar="STRING")

        args=parser.parse_args()
        gen_stop_times(args.trip_id,args.infile)

