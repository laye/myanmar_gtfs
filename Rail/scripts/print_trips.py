#
#
#   Generate a report of all trips for a given agency from a set of gtfs files 
#   Required Input files:
#   stops
#   agency
#   routes
#   stop_times
#   calendar 
#  
#   script needs to be run in the same folder as the gtfs files 
#   Usage:
#   >python print_trip.py -a {agency_id}  > outfile.csv 
#
#   where agency_id is the ID from the agency file, e.g. A0001 = Air Bagan 
#
#
#   l. Aye - Dec 2014 
#   ---------------------------------------------------------------------------

import sys,os,csv,time
import argparse


def print_routes(op_type,out_file):


    daysofweek=["Mon","Tue","Wed","Thu","Fri","Sat","Sun"]


    # Read and process all the gtfs files  
    agency_file=csv.reader(open("agency.csv", "r"),delimiter=",")
    stops_file = csv.reader(open("stops.csv", "r"),delimiter=",")
    trips_file = csv.reader(open("trips.csv", "r"),delimiter=",")
    routes_file= csv.reader(open("routes.csv","r"),delimiter=",")
    stoptimes_file = csv.reader(open("stop_times.csv", "r"), delimiter= ",")
    calendar_file=csv.reader(open("calendar.csv", "r"), delimiter= ",")


    #Creates an empty dictionaries for trips and routes 
    agency={}
    trips= {}
    routes={}
    stops={}
    stop_times={}
    calendar={}
    print 'optype',op_type
    #Read in stops file and build kye/value pairs for stop_id and stop_desc
    for st in stops_file:
        stops[st[0]]=st[3]
        
    #Read in agency file and build key/value pairs of agency_id and agency_name 
    for ag in agency_file:
        agency[ag[0]] = ag[1]
        
    #Read in routes file and build key/value pairs of routeId and agencyId
    for rt in routes_file:
        routes[rt[0]] = rt[1]

    #Read in calendar file and build key/value pairs of serviceID and dates
    for cal in calendar_file:
        calendar[cal[0]]=[cal[1],cal[2],cal[3],cal[4],cal[5],cal[6],cal[7]]

    #Read in stop_times file 
    #tripID field of the format: TA0101_W9-201 
    for stt in stoptimes_file:
        week=''
        if stt[0]!= 'trip_id':          #ignore header row
            routecode= "R"+stt[0][1:6]  #extract the routeID from trip_id field of stop_times file 
            agencyid=routes[routecode]  #extract the agency based on the routecode 
            service=stt[0][7:]          #extract service_id from trip_id field 
            stop=stt[3]                 #stopId is the 4th field in stop_times 
            
            if (op_type== agencyid) or (op_type=="all"):           
                #Print trip summary at the start of the trip, i.e. stop_sequence = 1
                if stt[4] == "1":           
                    #convert calendar into days
                    for i in range(0,len(calendar[service])):
                        if calendar[service][i] == "1": 
                            week=week+daysofweek[i]
                    if week == "MonTueWedThuFriSatSun":
                        week = "Daily"

                    print "\n", agency[agencyid], ",", service, ",", week 
                    print stops[stop],',',stt[1]
                else:
                    if len(stt[1])>5:   #reformat HH:MM for times >24hr clock
                        hr1=int(stt[1][:2])-24
                        hrstr1="0"+str(hr1)+":"+stt[1][3:5]
                        hr2=int(stt[2][:2])-24
                        hrstr2="0"+str(hr2)+":"+stt[2][3:5]
                        if hrstr1 == hrstr2:
                            print stops[stop],',', hrstr1 
                        else:
                            print stops[stop],',',hrstr1,',',hrstr2
                    else:
                        if stt[1] == stt[2]:
                            print stops[stop],',',stt[1]
                        else:
                            print stops[stop],',',stt[1],',',stt[2]
                



    # with open(out_file,'w') as csvfile:
        # annotated = csv.writer(csvfile,delimiter=',')
        
        # for trrow in stoptimes_f:
            # # Writes out to output file the same values from stop_times but stop_id field is 
            # # replaced by concatinating stop_id abd stop_code (from stops file)
            # stimesnew.writerow([newtrid[trrow[0]],trrow[1],trrow[2],trrow[3],trrow[4]])
            
            
        
#-----------------------------------------------------------------------------------------------------------------------
#   MAIN
#-----------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':

        parser = argparse.ArgumentParser()

        parser.add_argument("-a", "--agency", dest="op_type", help="agency_id from agency file", metavar="STRING")
        parser.add_argument("-f", "--file", dest="out_file", help="Output file", metavar="STRING")

        args=parser.parse_args()
        print_routes(args.op_type,args.out_file)
