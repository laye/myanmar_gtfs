import csv

# Read and process the trips file 
# i.e. strip the stopId and stop_code 
trips_file = csv.reader(open("trips.csv", "r"),delimiter=',')

#Creates an empty dictionaries for trips and routes 
trips= {}
routes={}
newtrid={}

with open('trips_new.csv','w') as csvfile:
    rnew = csv.writer(csvfile,delimiter=',') 

    for row in trips_file:
        #store key/value pairs of routeId and serviceId
        routes[row[0]]=row[1]
        #store key/value pairs of tripId and routeId 
        trips[row[2]]=row[0]
        newtrid[row[2]]="T"+ row[0][1:]+"_"+row[1]
        print row[0],row[1],newtrid[row[2]],row[3],row[4],row[5],row[6],row[7],row[8],row[9]
        rnew.writerow([row[0],row[1],newtrid[row[2]],row[3],row[4],row[5],row[6],row[7],row[8],row[9]])
    
#print routes
#print trips
#print newtrid

#Reads the stop_times file and replaces the stop_id with newly created stop_ids from array st 
stoptimes_f = csv.reader(open("stop_times.csv", "r"), delimiter= ",")

newsttimes=[]

with open('stop_times_new.csv','w') as csvfile:
    stimesnew = csv.writer(csvfile,delimiter=',')
    
    for trrow in stoptimes_f:
        # Writes out to output file the same values from stop_times but stop_id field is 
        # replaced by concatinating stop_id abd stop_code (from stops file)
        stimesnew.writerow([newtrid[trrow[0]],trrow[1],trrow[2],trrow[3],trrow[4]])
