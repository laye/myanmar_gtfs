import csv

# Read and process the stops file 
# i.e. strip the stopId and stop_code 
stops_file = csv.reader(open("stops.csv", "r"),delimiter=',')

#Creates an empty dictionary st 
st= {}
for row in stops_file:
    #store key/value pairs of stopID and stop_code into the dictionary 
    st[row[0]]=row[1]

print st 

#Reads the stop_times file and replaces the stop_id with newly created stop_ids from array st 
stoptimes_f = csv.reader(open("stop_times.csv", "r"), delimiter= ",")

newsttimes=[]

with open('stop_times_new.csv','w') as csvfile:
    stimesnew = csv.writer(csvfile,delimiter=',')
    
    for strow in stoptimes_f:
        # Writes out to output file the same values from stop_times but stop_id field is 
		# replaced by concatinating stop_id abd stop_code (from stops file)
        stimesnew.writerow([strow[0],strow[1],strow[2],strow[3]+"_"+st[strow[3]],strow[4]])